# Node.js concurrency

This tiny application is an experiment created in order to explore the subject of concurrency in Node.js.

## Installation

```
npm install
```

## Running the server
 
The application server can be run either as a single process or as a cluster of child processes working independently.

In order to run the simpler, single process version, you can use the following commands:

```
npm start
```

or

```
npm run simple
```

The multiprocess (cluster) version can be run using:

```
npm run cluster
```

## Available endpoints

There are three endpoints available in the test server application:

### /light

This endpoint uses `setTimeout()` to delay the returning of the response by a few seconds. CPU resources are not
explicitly used here.

In both server versions, concurrent requests to this endpoint should be received more or less simultaneously. CPU cores
should not receive any significant load.

### /heavy

This endpoint uses a naive and CPU intensive implementation of the Fibonacci sequence lookup. It calculates a number
fairly high in the sequence, spending lots of CPU resources.

The simple server version should yield a cascade of successive responses, received in approximately even intervals. This
is because in a single process + single thread, the concurrent requests will be processed sequentially. A single CPU
core should receive 100% load during the processing of all the requests.

With multiple processes, this endpoint will yield simultaneous responses. Each should be processed by a different CPU
core (so provided the number of requests is less or equal the number of cores, the number of busy cores will equal the
number of requests).

### /heavy-threads

This endpoint uses `webworker-threads` to produce a pool of as many child threads as there are CPU cores. Each request
should be processed by a different thread unless there are no free threads. The responses should arrive simultaneously
in both server implementations.

Also, both implementations should distribute the CPU load between all available cores (as many as there are requests).

## Performing the tests

In order to perform the tests, you can open the file **web/index.html** in a browser. The HTML file will allow you to
send series of concurrent requests to three available endpoints in the application. You can use the developer tools (aka
F12 tools) to monitor the requests.

It is also interesting to see how the CPU load is distributed. Tools such as `htop` can be used to preview the processes
that are running as well as the load on every CPU core.
