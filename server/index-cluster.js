const http = require("http");
const cluster = require("cluster");
const port = process.env.PORT || 3001;
const nbProcesses = require("os").cpus().length;

if (cluster.isMaster) {
	for (var i = 0; i < nbProcesses; ++i) {
		cluster.fork();
	}
	console.log(`Listening on port ${port}, PID: ${process.pid}`);
} else {
	http.createServer((req, res) => {
		res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,PUT,POST,DELETE");
		res.setHeader("Access-Control-Allow-Origin", "*");

		console.log(`Received request: ${req.url} (PID: ${process.pid})`);
		require(`./routes${req.url}`)(req, res, new Date());
	}).listen(port, () => {
		console.log(`Spawned worker process, PID: ${process.pid}`);
	});
}
