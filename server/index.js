const http = require("http");
const port = process.env.PORT || 3000;

http.createServer((req, res) => {
	res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,PUT,POST,DELETE");
	res.setHeader("Access-Control-Allow-Origin", "*");

	console.log(`Received request: ${req.url}`);
	require(`./routes${req.url}`)(req, res, new Date());
}).listen(port, () => {
	console.log(`Listening on port ${port}`);
});
