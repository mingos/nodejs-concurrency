module.exports = (req, res, date) => {
	setTimeout(() => {
		res.statusCode = 204;
		res.end();

		var time = (new Date() - date).valueOf();
		console.log(`Request processed after ${time} ms.`);
	}, 3000);
};
