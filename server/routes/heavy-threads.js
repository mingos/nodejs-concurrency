function fibonacci(pos) {
	return (pos > 2) ? fibonacci(pos - 2) + fibonacci(pos - 1) : 1;
}

var pool = require("webworker-threads").createPool(require("os").cpus().length);
pool.all.eval(fibonacci);

module.exports = (req, res, date) => {
	pool.any.eval("fibonacci(42);", () => {
		res.statusCode = 204;
		res.end();

		var time = (new Date() - date).valueOf();
		console.log(`Request processed after ${time} ms.`);
	});
};
