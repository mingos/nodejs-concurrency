function fibonacci(pos) {
	return (pos > 2) ? fibonacci(pos - 2) + fibonacci(pos - 1) : 1;
}

module.exports = (req, res, date) => {
	res.statusCode = 204;
	fibonacci(42);
	res.end();

	var time = (new Date() - date).valueOf();
	console.log(`Request processed after ${time} ms.`);
};
